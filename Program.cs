﻿using System;
using System.Collections.Generic;

namespace Laboratorio4
{
    class Program
    {
        static void Main(string[] args)
        {
            Circulo circ1 = new Circulo();
            Console.WriteLine(circ1);
            Circulo circ2 = new Circulo(2.4, 5, 3);
            Console.WriteLine(circ2);
            CirculoColorido circ3 = new CirculoColorido();
            Console.WriteLine(circ3);
            CirculoColorido circ4 = new CirculoColorido(1.5, 3.1, 1, "pink");
            Console.WriteLine(circ4);

            Console.WriteLine($"x = {circ4.CentroX} y = {circ4.CentroY}");

            CirculoColoridoPreenchido circ5 = new CirculoColoridoPreenchido(2.0, 4.5, 2, "pink", "DeepPink");
            Console.WriteLine(circ5);

            List<Circulo> lista = new List<Circulo>();
            lista.Add(circ1);
            lista.Add(circ2);
            lista.Add(circ3);
            lista.Add(circ4);
            lista.Add(circ5);

            lista.ForEach( c => Console.WriteLine( c ));

        }
    }
}
