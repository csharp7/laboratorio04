using System;
using System.Drawing;


public class CirculoColoridoPreenchido : CirculoColorido
{
    private Color cor2;

    public Color Cor2 {
        get {
            return cor2;
        }
        set{
            cor2 = value;
        }
    }

    public CirculoColoridoPreenchido(double x, double y, double r, string c, string c2)
        : base(x, y, r, c)
    {
        cor2 = Color.FromName(c2);
    }

    public override string ToString()
    {
        return base.ToString() + " cor2 = " + Cor2;
    }
}