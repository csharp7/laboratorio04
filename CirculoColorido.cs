using System;
using System.Drawing;

public class CirculoColorido : Circulo
{
    private Color cor = new Color();

    public Color Cor {
        get {
            return cor;
        }
        set{
            cor = value;
        }
    }

    public CirculoColorido()
    {
        cor = Color.FromName("Black");
    }

    public CirculoColorido(double x, double y, double r, string c)
        : base(x, y, r)
    {
        cor = Color.FromName(c);
    }

    public override string ToString()
    {
        return base.ToString() + " cor = " + Cor;
    }
}